pushd src/tonika

pushd needle/proto
make
popd

for i in $(find -name *go -exec grep -l ^package\ main {} \; | sed s/^..//g | sed s,/.*,, | sort | uniq) ; do
	pushd $i
	go install # -a
	popd
done

popd
