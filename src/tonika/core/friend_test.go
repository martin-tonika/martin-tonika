// Tonika: A distributed social networking platform
// Copyright (C) 2010 Petar Maymounkov <petar@5ttt.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package core

import (
	//"fmt"
	"testing"
	"tonika/sys"
)

func TestFriend(t *testing.T) {
	fdb0, err := MakeFriendDb("test.db")
	if fdb0 == nil || err != nil {
		t.Fatalf("Error making\n")
	}
	fdb0.GetMe().Name = "Petar"
	fdb0.GetMe().Addr = "serdika.isp.nah"
	id1 := sys.Id(87598375)
	fdb0.Attach(1234, &friend{
		sys.Friend{
			Id:   &id1,
			Name: "Chris",
			Addr: "145cpw",
		},
		false,
	})

	id2 := sys.Id(122938129)
	fdb0.Attach(1235, &friend{
		sys.Friend{
			Id:   &id2,
			Name: "Jennie",
			Addr: "nyny",
		},
		false,
	})
	err = fdb0.Save()
	if err != nil {
		t.Fatalf("Error saving: %v\n", err)
	}

	fdb, err := ReadFriendDb("test.db")
	if fdb == nil {
		t.Fatalf("Open friend db error")
	}
	if len(fdb.Enumerate()) != 2 {
		t.Errorf("Bad # of parsed friends")
	}
	jennie := fdb.GetById(122938129)
	if jennie.Name != "Jennie" {
		t.Errorf("Incorrect friend entries\n")
	}
}
