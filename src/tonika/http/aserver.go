// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package http

import (
	"container/list"
	"net"
	"os"
	"syscall"
	//"sync"
	"time"
	sync "tonika/prof"
)

// AsyncServer automates the reception of incoming HTTP connections
// at a given net.Listener. AsyncServer accepts new connections and
// manages each one with an AsyncServerConn object. AsyncServer also
// makes sure that a pre-specified limit of active connections (i.e.
// file descriptors) is not exceeded.
type AsyncServer struct {
	tmo    time.Duration // keepalive timout
	listen net.Listener
	conns  map[*stampedServerConn]int
	qch    chan *Query
	qchCls bool
	fdl    FDLimiter
	allow  *AllowHosts
	lk     sync.Mutex
}

type stampedServerConn struct {
	*AsyncServerConn
	stamp time.Time
	lk    sync.Mutex
}

func newStampedServerConn(c net.Conn) *stampedServerConn {
	return &stampedServerConn{
		AsyncServerConn: NewAsyncServerConn(c),
		stamp:           time.Now(),
	}
}

func (ssc *stampedServerConn) touch() {
	ssc.lk.Lock()
	defer ssc.lk.Unlock()
	ssc.stamp = time.Now()
}

func (ssc *stampedServerConn) GetStamp() time.Time {
	ssc.lk.Lock()
	defer ssc.lk.Unlock()
	return ssc.stamp
}

func (ssc *stampedServerConn) Read() (req *Request, err error) {
	ssc.touch()
	defer ssc.touch()
	return ssc.AsyncServerConn.Read()
}

func (ssc *stampedServerConn) Write(req *Request, resp *Response) (err error) {
	ssc.touch()
	defer ssc.touch()
	return ssc.AsyncServerConn.Write(req, resp)
}

// Incoming requests are presented to the user as a Query object.
// Query allows users to response to a request and to hijack the
// underlying AsyncServerConn, which is typically needed for CONNECT
// requests.
type Query struct {
	as            *AsyncServer
	ssc           *stampedServerConn
	req           *Request
	err           error
	fwd, hijacked bool
}

// NewAsyncServer creates a new AsyncServer which listens for connections on l.
// New connections are automatically managed by AsyncServerConn objects with
// timout set to tmo nanoseconds. The AsyncServer object ensures that at no
// time more than fdlim file descriptors are allocated to incoming connections.
func NewAsyncServer(l net.Listener, tmo time.Duration, fdlim int) *AsyncServer {
	if tmo < 2 {
		panic("as, timeout too small")
	}
	// TODO(petar): Perhaps a better design passes the FDLimiter as a parameter
	as := &AsyncServer{
		tmo:    tmo,
		listen: l,
		conns:  make(map[*stampedServerConn]int),
		qch:    make(chan *Query),
	}
	as.fdl.Init(fdlim)
	go as.acceptLoop()
	go as.expireLoop()
	return as
}

func (as *AsyncServer) GetFDLimiter() *FDLimiter { return &as.fdl }

func (as *AsyncServer) expireLoop() {
	for {
		as.lk.Lock()
		if as.listen == nil {
			as.lk.Unlock()
			return
		}
		now := time.Now()
		kills := list.New()
		for ssc, _ := range as.conns {
			if now.Sub(ssc.GetStamp()) >= as.tmo {
				kills.PushBack(ssc)
			}
		}
		as.lk.Unlock()
		elm := kills.Front()
		for elm != nil {
			ssc := elm.Value.(*stampedServerConn)
			as.bury(ssc)
			elm = elm.Next()
		}
		kills.Init()
		kills = nil
		time.Sleep(as.tmo)
	}
}

func (as *AsyncServer) SetAllowHosts(ah *AllowHosts) {
	as.lk.Lock()
	defer as.lk.Unlock()
	as.allow = ah
}

func (as *AsyncServer) isAllowed(c net.Conn) bool {
	as.lk.Lock()
	defer as.lk.Unlock()
	if as.allow == nil {
		return true
	}
	return as.allow.IsAllowedAddr(c.RemoteAddr())
}

func (as *AsyncServer) acceptLoop() {
	for {
		as.lk.Lock()
		l := as.listen
		as.lk.Unlock()
		if l == nil {
			return
		}
		as.fdl.Lock()
		c, err := as.listen.Accept()
		if err != nil || !as.isAllowed(c) {
			if err == nil {
				err = syscall.EPERM
			}
			if c != nil {
				c.Close()
			}
			as.fdl.Unlock()
			as.qch <- &Query{err: err}
			continue
		}
		c.(*net.TCPConn).SetKeepAlive(true)
		err = nil //c.SetReadTimeout(as.tmo) //TODO: how to replace this correctly with SetDeadline?
		if err != nil {
			c.Close()
			as.fdl.Unlock()
			as.qch <- &Query{err: err}
			return
		}
		c = NewConnRunOnClose(c, func() { as.fdl.Unlock() })
		ssc := newStampedServerConn(c)
		ok := as.register(ssc)
		if !ok {
			ssc.Close()
			c.Close()
		}
		go as.read(ssc)
	}
}

// Read() waits until a new request is received. The request is
// returned in the form of a Query object. A returned error
// indicates that the AsyncServer cannot accept new connections,
// and the user us expected to call Shutdown(), perhaps after serving
// outstanding queries.
func (as *AsyncServer) Read() (query *Query, err error) {
	q, ok := <-as.qch
	as.lk.Lock()
	if !ok {
		as.lk.Unlock()
		return nil, syscall.EBADF
	}
	as.lk.Unlock()
	if err = q.getError(); err != nil {
		return nil, err
	}
	return q, nil
}

func (q *Query) getError() error { return q.err }

// GetRequest() returns the underlying request. The result
// is never nil.
func (q *Query) GetRequest() *Request { return q.req }

// Continue() indicates to the AsyncServer that it can continue
// listening for incoming requests on the AsyncServerConn that
// delivered the request underlying this Query object.
// For every query returned by AsyncServer.Read(), the user must
// call either Continue() or Hijack(), but not both, and only once.
func (q *Query) Continue() {
	if q.fwd {
		panic("as, query, continue/hijack")
	}
	q.fwd = true
	go q.as.read(q.ssc)
}

// Hijack() instructs the AsyncServer to stop managing the AsyncServerConn
// that delivered the request underlying this Query. The connection is returned
// and the user becomes responsible for it.
// For every query returned by AsyncServer.Read(), the user must
// call either Continue() or Hijack(), but not both, and only once.
func (q *Query) Hijack() *AsyncServerConn {
	if q.fwd {
		panic("as, query, continue/hijack")
	}
	q.fwd = true
	q.hijacked = true
	as := q.as
	q.as = nil
	ssc := q.ssc
	q.ssc = nil
	as.unregister(ssc)
	return ssc.AsyncServerConn
}

// Write sends resp back on the connection that produced the request.
// Any non-nil error returned pertains to the AsyncServerConn and not
// to the AsyncServer as a whole.
func (q *Query) Write(resp *Response) (err error) {
	req := q.req
	q.req = nil
	err = q.ssc.Write(req, resp)
	if err != nil {
		q.as.bury(q.ssc)
		q.ssc = nil
		q.as = nil
		// TODO,XXX: This addition (below) here seems to fix a bug, but 
		// it makes no sense since bury (above) closes the ASC which closes all 
		// outstanding bodies.
		if resp.Body != nil {
			resp.Body.Close()
		}
		return
	}
	return
}

func (q *Query) LocalAddr() net.Addr {
	if q.ssc != nil {
		return q.ssc.Conn().LocalAddr()
	}
	return nil
}

func (q *Query) RemoteAddr() net.Addr {
	if q.ssc != nil {
		return q.ssc.Conn().RemoteAddr()
	}
	return nil
}

func (as *AsyncServer) read(ssc *stampedServerConn) {
	for {
		req, err := ssc.Read()
		perr, ok := err.(*os.PathError)
		if ok && perr.Err == syscall.EAGAIN {
			as.bury(ssc)
			return
		}
		if err != nil {
			// TODO(petar): Technically, a read side error should not terminate
			// the ASC, if there are outstanding requests to be answered,
			// since the write side might still be healthy. But this is
			// virtually never the case with TCP, so we currently go for simplicity
			// and just close the connection.
			as.bury(ssc)
			return
		}
		as.qch <- &Query{as, ssc, req, nil, false, false}
		return
	}
}

func (as *AsyncServer) register(ssc *stampedServerConn) bool {
	as.lk.Lock()
	defer as.lk.Unlock()
	if as.qchClosed() {
		return false
	}
	if _, present := as.conns[ssc]; present {
		panic("as, register twice")
	}
	as.conns[ssc] = 1
	return true
}

func (as *AsyncServer) unregister(ssc *stampedServerConn) {
	as.lk.Lock()
	defer as.lk.Unlock()
	delete(as.conns, ssc)
}

func (as *AsyncServer) bury(ssc *stampedServerConn) {
	as.unregister(ssc)
	c, _, _ := ssc.Close()
	if c != nil {
		c.Close()
	}
}

func (as *AsyncServer) closeQch() {
	close(as.qch)
	as.qchCls = true
}

func (as *AsyncServer) qchClosed() bool {
	return as.qchCls;
}

// Shutdown closes the AsyncServer by closing the underlying
// net.Listener object. The user should not use any AsyncServer
// or Query methods after a call to Shutdown.
func (as *AsyncServer) Shutdown() (err error) {
	// First, close the listener
	as.lk.Lock()
	var l net.Listener
	l, as.listen = as.listen, nil
	as.closeQch()
	as.lk.Unlock()
	if l != nil {
		err = l.Close()
	}
	// Then, force-close all open connections
	as.lk.Lock()
	for ssc, _ := range as.conns {
		c, _, _ := ssc.Close()
		if c != nil {
			c.Close()
		}
		delete(as.conns, ssc)
	}
	as.lk.Unlock()
	return
}
