pushd src/tonika

for i in $(ls) ; do
	pushd $i
	go test || exit -1
	popd
done

popd
